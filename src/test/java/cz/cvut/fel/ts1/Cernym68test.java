package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Cernym68test {

    @Test
    public void faktorialTest() {
        Main cernym68test = new Main();
        int num = 3;
        long expectedResult = 6;
        long result = cernym68test.factorial(num);

        Assertions.assertEquals(expectedResult, result);
    }
}

